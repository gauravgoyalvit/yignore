package com.yignore.yignore.Models;

import android.content.Intent;
import android.graphics.Bitmap;

/**
 * Created by gaurav on 05/09/15.
 */
public class ChatItem {

    public String getChatText() {
        return chatText;
    }

    public void setChatText(String chatText) {
        this.chatText = chatText;
    }

    public Boolean getFlag() {
        return flag;
    }

    public void setFlag(Boolean flag) {
        this.flag = flag;
    }

    public ChatItem(String chatText, Boolean flag,String type,Bitmap bitmap) {
        this.chatText = chatText;
        this.flag = flag;
        this.type = type;
        this.bitmap = bitmap;
    }

    public String chatText;

    public Boolean flag;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String type;

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap bitmap;
}
