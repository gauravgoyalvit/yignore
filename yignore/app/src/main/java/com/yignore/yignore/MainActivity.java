package com.yignore.yignore;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SeekBar;

import android.widget.Spinner;

import com.yignore.yignore.Models.ChatItem;
import com.yignore.yignore.Utilities.Constants;
import com.yignore.yignore.adapters.ChatListItemAdapter;
import com.yignore.yignore.customwidgets.MultiSliderWidget;

import java.util.ArrayList;
import java.util.List;

import fr.ganfra.materialspinner.MaterialSpinner;


public class MainActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    String[] myDataset = {"complain1","complain2"};
    private SeekBar seekBar;
    private static final int RESULT_LOAD_IMAGE = 1;
    private List<ChatItem> list = new ArrayList<>();
    private ChatListItemAdapter chatListItemAdapter;
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        // chat screen
        setContentView(R.layout.chat_list);

         listView = (ListView)findViewById(R.id.my_list);


        list.add(new ChatItem("text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1" +
                " text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text " +
                "text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text1 text",true, Constants.textType,null));
        list.add(new ChatItem("text1",false,Constants.textType,null));
        list.add(new ChatItem("text2",true,Constants.textType,null));
        list.add(new ChatItem("text2",false,Constants.textType,null));
        list.add(new ChatItem("text3",true,Constants.textType,null));
        list.add(new ChatItem("text3",false,Constants.textType,null));
        list.add(new ChatItem("text4",true,Constants.textType,null));
        list.add(new ChatItem("text4",false,Constants.textType,null));
        list.add(new ChatItem("text5",true,Constants.textType,null));
        list.add(new ChatItem("text5",false,Constants.textType,null));
        list.add(new ChatItem("text6",true,Constants.textType,null));
        list.add(new ChatItem("text6",false,Constants.textType,null));
        list.add(new ChatItem("text7",true,Constants.textType,null));
        list.add(new ChatItem("text7",false,Constants.textType,null));
        list.add(new ChatItem("text8",true,Constants.textType,null));
        list.add(new ChatItem("text8",false,Constants.textType,null));
        list.add(new ChatItem("text9",true,Constants.textType,null));
        list.add(new ChatItem("text9", false,Constants.textType,null));
        list.add(new ChatItem("text10", true,Constants.textType,null));
        list.add(new ChatItem("text10", false,Constants.textType,null));

        chatListItemAdapter = new ChatListItemAdapter(this,list);
        listView.setAdapter(chatListItemAdapter);
        listView.setSelection(list.size() - 1);


        View footerView = findViewById(R.id.footbar);

        ImageView imageView = (ImageView)footerView.findViewById(R.id.send);
        final EditText editText = (EditText)footerView.findViewById(R.id.edit_text);
        final List<ChatItem> chatItemList = list;
        final ListView listView1 = listView;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // add message to list
                if(!editText.getText().toString().equals("")){
                chatItemList.add(new ChatItem(editText.getText().toString(), true,Constants.textType,null));

                editText.setText("");
                chatListItemAdapter.notifyDataSetChanged();
                listView1.setSelection(chatItemList.size() - 1);}
                // add an image
                else {

                            Intent i = new Intent(
                                    Intent.ACTION_PICK,
                                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI);


                    startActivityForResult(i, RESULT_LOAD_IMAGE);


                }
            }
        });

        final ImageView imageView1 = imageView;
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable string) {
                if (string.length() > 0) {
                    imageView1.setImageResource(R.drawable.senticon);
                } else {
                    imageView1.setImageResource(R.drawable.imageicon1);
                }
            }
        });





        // new complain screen.--3rd Screen
//        setContentView(R.layout.new_complain);
//
//        MaterialSpinner spinner = (MaterialSpinner) findViewById(R.id.category_spinner);
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
//                R.array.category_array, R.layout.spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        spinner.setAdapter(adapter);
//
//
//        MultiSliderWidget multiSliderWidget = (MultiSliderWidget)findViewById(R.id.multi_slider);
//        multiSliderWidget.markStep(3);
//
//        ArrayAdapter<CharSequence> categoryAdapter = ArrayAdapter.createFromResource(this,
//                R.array.product_array, R.layout.spinner_item);
//        categoryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        MaterialSpinner spinner1 = (MaterialSpinner) findViewById(R.id.spinner);
//        spinner1.setAdapter(categoryAdapter);



        // complain list screen -- 2nd screen
//        mRecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
//        mRecyclerView.setHasFixedSize(true);
//        mLayoutManager = new LinearLayoutManager(this);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//        mAdapter = new ComplainListItemAdapter(myDataset);
//        mRecyclerView.setAdapter(mAdapter);


        // home screen
//        FloatingActionButton floatingActionButton = (FloatingActionButton)findViewById(R.id.fab);
//        floatingActionButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(MainActivity.this,"ADD New is clicked",Toast.LENGTH_SHORT);
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {

            Uri uri = data.getData();

            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
              list.add(new ChatItem(null, true, Constants.imageType, bitmap));
                chatListItemAdapter.notifyDataSetChanged();
                listView.setSelection(list.size() - 1);

            } catch (Exception e) {
                ;
            }
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
