package com.yignore.yignore.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yignore.yignore.Models.ChatItem;
import com.yignore.yignore.R;
import com.yignore.yignore.Utilities.Constants;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by gaurav on 29/08/15.
 */

public class ChatListItemAdapter extends BaseAdapter {
    private Activity context;
    private LayoutInflater inflater;
    private List<ChatItem> mDataset;


    @Override
    public int getCount() {
        return mDataset.size();
    }


    public ChatListItemAdapter(Activity context,List<ChatItem> list) {
        this.mDataset = list;
        inflater = context.getLayoutInflater();
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        ChatItem chatItem = mDataset.get(position);
        if (chatItem.getType().equals(Constants.textType) ) {
            if(chatItem.getFlag())
            return 0;
            else return 1;
        } else
        if(chatItem.getFlag())
            return 2;
        else return 3;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int type = getItemViewType(position);

        // ViewHolder holder = null;

        if (convertView == null)

        {
            if (type == 0) {
                convertView = inflater.inflate(R.layout.chat_list_item_left,
                        parent, false);
                setAlign(convertView.findViewById(R.id.chat_text),true);
            } else if (type == 1) {

                convertView = inflater.inflate(R.layout.chat_list_item_left, parent, false);
                setAlign(convertView.findViewById(R.id.chat_text),false);
            }
            else if (type == 2) {

                convertView = inflater.inflate(R.layout.chat_item_image_view, parent, false);
                setAlign(convertView.findViewById(R.id.image_view),true);
            }
            else if (type == 3) {

                convertView = inflater.inflate(R.layout.chat_item_image_view, parent, false);
                setAlign(convertView.findViewById(R.id.image_view),false);
            }

        }

        if(type==0 || type==1){
            TextView textView = (TextView)convertView.findViewById(R.id.chat_text);

            textView.setText(mDataset.get(position).getChatText());
        //    setAlign(textView,mDataset.get(position).getFlag());
        }

        if(type==2){
            ImageView imageView = (ImageView)convertView.findViewById(R.id.image_view);
            Bitmap bitmap = getResizedBitmap(mDataset.get(position).getBitmap(),20,20);
            imageView.setImageBitmap(bitmap);
           // setAlign(imageView,mDataset.get(position).getFlag());

        }

        return convertView;


    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mDataset.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setAlign(View v,Boolean align){
        RelativeLayout.LayoutParams params_ = (RelativeLayout.LayoutParams)v.getLayoutParams();
        if(align) {
            params_.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        }
        else {
            params_.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }
        v.setLayoutParams(params_);}

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }



    }



