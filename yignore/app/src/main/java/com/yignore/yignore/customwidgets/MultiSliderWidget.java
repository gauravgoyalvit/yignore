package com.yignore.yignore.customwidgets;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.yignore.yignore.R;


/**
 * Created by ggoyal on 6/7/15.
 */
public class MultiSliderWidget extends RelativeLayout {

private TextView step1;
    private TextView step2;
    private TextView step3;
    private TextView step4;
    private MultiSlider multiSlider;


    public MultiSliderWidget(Context context) {
        super(context);
        build(context);
    }

    public MultiSliderWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        build(context);
    }

    private void build(Context context) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.multiple_steps_slider, this, true);
        step1 = (TextView)findViewById(R.id.step1);
        step2 = (TextView)findViewById(R.id.step2);
        step3 = (TextView)findViewById(R.id.step3);
        step4 = (TextView)findViewById(R.id.step4);
        multiSlider = (MultiSlider)findViewById(R.id.range_slider5);

    }

    public void markStep(int index){
        multiSlider.setHighlight(index);
        switch (index){
            case 1:
                step1.setTextColor(Color.GREEN);
                break;
            case 2:
                step2.setTextColor(Color.GREEN);
                break;
            case 3:
                step3.setTextColor(Color.GREEN);
                break;
            case 4:
                step4.setTextColor(Color.GREEN);
                break;

        }
    }







}
